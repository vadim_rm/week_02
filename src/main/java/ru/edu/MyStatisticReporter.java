package ru.edu;


import java.io.*;

import java.nio.charset.StandardCharsets;


public class MyStatisticReporter implements StatisticReporter {


    /**

     * Файл с отчетом.

     */


    private File file;


    /**

     * Конструктор с указанием пути для отчета.

     *

     * @param filePath This is the type parameter

     */

    public MyStatisticReporter(final String filePath) {


        file = new File(filePath);

    }


    /**

     * Формирование отчета.

     *

     * @param statistics - данные статистики

     */

    @Override

    public void report(final TextStatistics statistics) {

        try (FileOutputStream os = new FileOutputStream(file);

             OutputStreamWriter sw = new OutputStreamWriter(

                     os,

                     StandardCharsets.UTF_8);


             PrintWriter writer = new PrintWriter(sw)) {


            writer.println("Words: " + statistics.getWordsCount());

            writer.println("Chars: " + statistics.getCharsCount());

            writer.println("NotSpace: "

                    + statistics.getCharsCountWithoutSpaces());

            writer.println("Puncts: "

                    + statistics.getCharsCountOnlyPunctuations());

        } catch (FileNotFoundException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }

    }



}