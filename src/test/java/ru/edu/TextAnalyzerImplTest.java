package ru.edu;


import org.junit.Test;


import static org.junit.Assert.assertEquals;


public class TextAnalyzerImplTest {


    @Test

    public void analyze() {

        TextAnalyzer analyzer = new MyTextAnalyzer();


        analyzer.analyze("12345");


        TextStatistics statistic = analyzer.getStatistic();


        assertEquals(1, statistic.getWordsCount());

        assertEquals(5, statistic.getCharsCount());

        assertEquals(5, statistic.getCharsCountWithoutSpaces());

        assertEquals(0, statistic.getCharsCountOnlyPunctuations());

    }


    @Test

    public void analyzeMultiline() {

        TextAnalyzer analyzer = new MyTextAnalyzer();


        analyzer.analyze("12345");

        analyzer.analyze("\n123, asd45");


        TextStatistics statistic = analyzer.getStatistic();


        assertEquals(3, statistic.getWordsCount());

        assertEquals(16, statistic.getCharsCount());

        assertEquals(15, statistic.getCharsCountWithoutSpaces());

        assertEquals(1, statistic.getCharsCountOnlyPunctuations());

    }

}