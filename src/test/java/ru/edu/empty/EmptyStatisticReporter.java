package ru.edu.empty;

import ru.edu.StatisticReporter;
import ru.edu.TextStatistics;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class EmptyStatisticReporter implements StatisticReporter {

    /**
     * Формирование отчета.
     *
     * @param statistics - данные статистики
     */
    @Override
    public void report(TextStatistics statistics) {
        File output = new File("./result.txt");


        try(FileOutputStream os = new FileOutputStream(output);
            OutputStreamWriter sw = new OutputStreamWriter(os, StandardCharsets.UTF_8);
            PrintWriter writer = new PrintWriter(sw)){
            writer.println("Результат должен быть здесь");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
