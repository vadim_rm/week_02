package ru.edu;


import org.junit.Test;


import java.util.ArrayList;

import java.util.List;


import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertNotNull;


public class MySourceReaderTest {


    private static final String EXIST_FILE = "./src/test/resources/input_text.txt";

    private static final String TEST_FILE = "./src/test/resources/testSourceReader.txt";

    public static final String NOT_EXIST_FILE_PATH = "./not_exist_file_path";


    private SourceReader reader = new MySourceReader();


    @Test

    public void setupExistFilePath() {


        reader.setup(EXIST_FILE);

    }


    @Test(expected = IllegalArgumentException.class)

    public void setupNull() {


        reader.setup(null);

    }


    @Test(expected = IllegalArgumentException.class)

    public void setupNotExistFile() {

        reader.setup(NOT_EXIST_FILE_PATH);

    }


    @Test

    public void readSource() {

        TestTextAnalyzer analyzer = new TestTextAnalyzer();


        reader.setup(TEST_FILE);

        TextStatistics statistics = reader.readSource(analyzer);


        assertNotNull(statistics);

        assertEquals(2, statistics.getWordsCount());


        assertEquals("first line", analyzer.lines.get(0));

        assertEquals("\nsecond line", analyzer.lines.get(1));

    }


    static class TestTextAnalyzer implements TextAnalyzer {


        public List<String> lines = new ArrayList<>();


        /**
         * Анализ строки произведения.
         *

         * @param line

         */

        @Override

        public void analyze(String line) {


            lines.add(line);

        }


        /**

         * Получение сохраненной статистики.

         *

         * @return TextStatistic

         */

        @Override

        public TextStatistics getStatistic() {

            TextStatistics statistics = new TextStatistics();

            statistics.addWordsCount(lines.size());

            return statistics;

        }


    }

}